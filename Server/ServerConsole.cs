﻿/*
© Siemens AG, 2017-2019
Author: Dr. Martin Bischoff (martin.bischoff@siemens.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using RosSharp.RosBridgeClient;

using std_msgs = RosSharp.RosBridgeClient.MessageTypes.Std;
using std_srvs = RosSharp.RosBridgeClient.MessageTypes.Std;
using rosapi = RosSharp.RosBridgeClient.MessageTypes.Rosapi;
using System.Net.Sockets;
using System.Net;
using ExUnity;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using RosSharp.RosBridgeClient.MessageTypes.Logistic;
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;
using RosSharp.RosBridgeClient.MessageTypes.Smach;
using System.Threading;
// commands on ROS system:
// launch before starting:
// roslaunch rosbridge_server rosbridge_websocket.launch
// rostopic echo /publication_test
// rostopic pub /subscription_test std_msgs/String "subscription test message data"

// launch after starting:
// rosservice call /service_response_test

namespace Server.ConsoleMain
{
    public class ServerConsole
    {

        public static MyServer MyServer;
        public static RosSocket rosSocket;
        public static RosSocket rosSocket2;

        private static Dictionary<string, Dictionary<string, string>> ParseIPConfig(string jsonFileName)
        {
            Dictionary<string, Dictionary<string, string>> ParsedIP = new Dictionary<string, Dictionary<string, string>>();

            string path = Path.Combine(new string[] { "../../../ExUnity", jsonFileName });
            string ConnectionSettingsJson = null;
            using (StreamReader r = new StreamReader(path))
            {
                ConnectionSettingsJson = r.ReadToEnd();
            }
            Dictionary<string, Dictionary<string, string>> connectionSettings = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(ConnectionSettingsJson);
            foreach (var kvp in connectionSettings)
            {
                if (kvp.Key.ToLower() == "ros")
                {
                    string rosIP = kvp.Value["ip"];
                    string rosPort = kvp.Value["port"];
                    string rosUri = $"ws://{rosIP}:{rosPort}";
                    ParsedIP["ros"] = new Dictionary<string, string>() { { "uri", rosUri } };
                }
                else
                {
                    ParsedIP[kvp.Key] = kvp.Value;
                }
            }

            return ParsedIP;
        }

        public static void MyMain()
        {
            string jsonFileName = "HololensConnectionSettings.json";
            Dictionary<string, Dictionary<string, string>> devicesIP = ParseIPConfig(jsonFileName);

            MyServer = new MyServer(devicesIP["hololens"]["ip"], devicesIP["hololens"]["port"]);
            MyServer.StartServer();



            rosSocket = new RosSocket(new RosSharp.RosBridgeClient.Protocols.WebSocketNetProtocol(devicesIP["ros"]["uri"]));
            rosSocket2 = new RosSocket(new RosSharp.RosBridgeClient.Protocols.WebSocketNetProtocol(devicesIP["ros"]["uri"]));

            //Thread serviceThread = new Thread(ServiceThreadHandler);
            //serviceThread.Start();

            // Subscription:
            string service_id = "";
            try
            {
                //subscription_id = rosSocket.Subscribe<LogisticTask>("/current_logistic_task", LogisticTaskSubscriptionHandler);
                service_id = rosSocket2.AdvertiseService<LogisticOperatorTaskRequest, LogisticOperatorTaskResponse>("/logistic_operator", ServiceResponseHandler);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Subscribe() error: " + ex.ToString());
            }


            //rosSocket.CallService<rosapi.GetParamRequest, rosapi.GetParamResponse>("/rosapi/get_param", ServiceCallHandler, new rosapi.GetParamRequest("/logistic_tasks", "default"));


            //string subscription_id = rosSocket.Subscribe<SmachContainerStatus>("/smach_server/smach/container_status", SubscriptionHandler);
            string subscription_id = rosSocket.Subscribe<ActiveLogisticTasks>("/active_logistic_tasks", SubscriptionHandler);

            Console.WriteLine("Press any key to unsubscribe...");
            Console.ReadKey(true);
            rosSocket.Unsubscribe(subscription_id);
            rosSocket.UnadvertiseService(service_id);
            //serviceThread.Abort();

            Console.WriteLine("Press any key to close...");
            Console.ReadKey(true);
            rosSocket.Close();
        }

        public static void Main(string[] args)
        {
            MyMain();
        }

        private static void SubscriptionHandler(ActiveLogisticTasks message)
        {
            Console.WriteLine("active logistic tasks:");
            foreach (LogisticTask logistic_task in message.active_logistic_tasks)
            {
                Console.WriteLine("- " + logistic_task.task_id + " : " + logistic_task.task_name);
            }
            Console.WriteLine();
            CommunicationMessage communicationMessage = new CommunicationMessage("server", "hololens", MessageType.ActiveLogisticTasks, message);
            if (MyServer != null)
            {
                MyServer.SendJsonThread(communicationMessage);
            }
        }


        private static bool ServiceResponseHandler(LogisticOperatorTaskRequest request, out LogisticOperatorTaskResponse result)
        {
            result = new LogisticOperatorTaskResponse();
            result.action_result.status.status = GoalStatus.PENDING;
            CommunicationMessage communicationMessage = new CommunicationMessage("server", "hololens", MessageType.LogisticTask, request.logistic_task);
            if (MyServer != null)
            {
                MyServer.SendJsonThread(communicationMessage);
            }

            // Logic: it sends to Hololens the operator's task to execute and wait for a response. The response arrive when the static property operatorGoalStatus is set properly (succeeded, aborted or preempted).
            while (true)
            {
                // assigning MessageHandler.operatorGoalStatus to a local variable is like locking the variable for this thread (I hope)
                GoalStatus operatorGoalStatus = MessageHandler.operatorGoalStatus;
                if (operatorGoalStatus.goal_id != null && operatorGoalStatus.goal_id.id != null && operatorGoalStatus.goal_id.id == request.logistic_task.task_id && (operatorGoalStatus.status == GoalStatus.SUCCEEDED || operatorGoalStatus.status == GoalStatus.ABORTED || operatorGoalStatus.status == GoalStatus.PREEMPTED))
                {
                    result.action_result.status.status = operatorGoalStatus.status;

                    Console.WriteLine($"setting {request.logistic_task.task_id} to {result.action_result.status.status}");
                    return true;
                }
            }
        }
    }
}