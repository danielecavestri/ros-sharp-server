﻿/* 
 * This message is custom
 */

namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class ActiveLogisticTasks : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/ActiveLogisticTasks";

        public LogisticTask[] active_logistic_tasks { get; set; }

        public ActiveLogisticTasks()
        {
            this.active_logistic_tasks = new LogisticTask[0];
        }

        public ActiveLogisticTasks(LogisticTask[] active_logistic_tasks)
        {
            this.active_logistic_tasks = active_logistic_tasks;
        }
    }
}
