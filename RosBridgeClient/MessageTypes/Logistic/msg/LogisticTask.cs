﻿/* 
 * This message is custom
 */

namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticTask : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/logistic_task";

        public string task_id { get; set; }
        public string task_name { get; set; }
        public string task_description { get; set; }
        public string task_type { get; set; }
        public string[] pieces_involved { get; set; }
        public string[] hardware_involved { get; set; }

        public LogisticTask()
        {
            this.task_id = "";
            this.task_name = "";
            this.task_description = "";
            this.task_type = "";
            this.pieces_involved = new string[0];
            this.hardware_involved = new string[0];
        }

        public LogisticTask(string task_id, string task_name, string task_description, string task_type, string[] pieces_involved, string[] hardware_involved)
        {
            this.task_id = task_id;
            this.task_name = task_name;
            this.task_description = task_description;
            this.task_type = task_type;
            this.pieces_involved = pieces_involved;
            this.hardware_involved = hardware_involved;
        }
    }
}
