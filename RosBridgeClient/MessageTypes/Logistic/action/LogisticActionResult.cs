using RosSharp.RosBridgeClient.MessageTypes.Std;
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;

namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticActionResult : ActionResult<LogisticResult>
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticActionResult";

        public LogisticActionResult() : base()
        {
            this.result = new LogisticResult();
        }

        public LogisticActionResult(Header header, GoalStatus status, LogisticResult result) : base(header, status)
        {
            this.result = result;
        }
    }
}
