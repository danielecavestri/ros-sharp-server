using RosSharp.RosBridgeClient.MessageTypes.Std;
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;

namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticActionFeedback : ActionFeedback<LogisticFeedback>
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticActionFeedback";

        public LogisticActionFeedback() : base()
        {
            this.feedback = new LogisticFeedback();
        }

        public LogisticActionFeedback(Header header, GoalStatus status, LogisticFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
    }
}
