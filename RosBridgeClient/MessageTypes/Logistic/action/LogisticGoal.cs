namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticGoal : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticGoal";

        //  goal definition
        public LogisticTask logistic_task;

        public LogisticGoal()
        {
            this.logistic_task = new LogisticTask();
        }

        public LogisticGoal(LogisticTask logisticTask)
        {
            this.logistic_task = logisticTask;
        }
    }
}
