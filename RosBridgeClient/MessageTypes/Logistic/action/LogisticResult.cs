namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticResult : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticResult";

        //  result definition

        public LogisticResult()
        {
        }

    }
}
