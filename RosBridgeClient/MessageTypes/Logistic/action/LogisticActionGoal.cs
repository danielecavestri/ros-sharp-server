using RosSharp.RosBridgeClient.MessageTypes.Std;
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;

namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticActionGoal : ActionGoal<LogisticGoal>
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticActionGoal";

        public LogisticActionGoal() : base()
        {
            this.goal = new LogisticGoal();
        }

        public LogisticActionGoal(Header header, GoalID goal_id, LogisticGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
    }
}
