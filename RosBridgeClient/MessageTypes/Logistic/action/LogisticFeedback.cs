namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticFeedback : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticFeedback";

        //  feedback

        public LogisticFeedback()
        {
        }

    }
}
