
namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticOperatorTaskTestRequest : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticOperatorTaskTest";

        public LogisticTask LogisticTask { get; set; }

        public LogisticOperatorTaskTestRequest()
        {
            this.LogisticTask = new LogisticTask();
        }

        public LogisticOperatorTaskTestRequest(LogisticTask logisticTask)
        {
            LogisticTask = logisticTask;
        }
    }
}
