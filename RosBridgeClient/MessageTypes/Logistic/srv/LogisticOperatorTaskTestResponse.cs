
namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticOperatorTaskTestResponse : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticOperatorTaskTest";

        public bool succeeded { get; set; }

        public LogisticOperatorTaskTestResponse()
        {
            this.succeeded = false;
        }

        public LogisticOperatorTaskTestResponse(bool succeeded)
        {
            this.succeeded = succeeded;
        }
    }
}
