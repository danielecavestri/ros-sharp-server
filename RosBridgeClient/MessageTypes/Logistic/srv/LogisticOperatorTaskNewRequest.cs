
namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticOperatorTaskNewRequest : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticOperatorTaskNew";

        public LogisticTask LogisticTask { get; set; }

        public LogisticOperatorTaskNewRequest()
        {
            this.LogisticTask = new LogisticTask();
        }

        public LogisticOperatorTaskNewRequest(LogisticTask logisticTask)
        {
            LogisticTask = logisticTask;
        }
    }
}
