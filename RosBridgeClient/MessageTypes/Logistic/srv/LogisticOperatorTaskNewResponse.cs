
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;

namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticOperatorTaskNewResponse : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticOperatorTaskNew";

        public GoalStatus status{ get; set; }

        public LogisticOperatorTaskNewResponse()
        {
            this.status = new GoalStatus();
        }

        public LogisticOperatorTaskNewResponse(GoalStatus status)
        {
            this.status = status;
        }
    }
}
