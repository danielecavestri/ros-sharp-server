
namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticOperatorTaskRequest : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticOperatorTask";

        public LogisticTask logistic_task { get; set; }

        public LogisticOperatorTaskRequest()
        {
            this.logistic_task = new LogisticTask();
        }

        public LogisticOperatorTaskRequest(LogisticTask logisticTask)
        {
            logistic_task = logisticTask;
        }
    }
}
