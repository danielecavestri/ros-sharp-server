
namespace RosSharp.RosBridgeClient.MessageTypes.Logistic
{
    public class LogisticOperatorTaskResponse : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/LogisticOperatorTask";

        public LogisticActionResult action_result { get; set; }

        public LogisticOperatorTaskResponse()
        {
            this.action_result = new LogisticActionResult();
        }

        public LogisticOperatorTaskResponse(LogisticActionResult actionResult)
        {
            action_result = actionResult;
        }
    }
}
