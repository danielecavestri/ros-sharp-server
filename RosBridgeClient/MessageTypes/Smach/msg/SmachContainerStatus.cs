﻿/* 
 * This message is a custom message
 */

using RosSharp.RosBridgeClient.MessageTypes.Std;

namespace RosSharp.RosBridgeClient.MessageTypes.Smach
{
    public class SmachContainerStatus : Message
    {
        public const string RosMessageName = "smach_msgs/SmachContainerStatus";

        public Header header { get; set; }
        public string path { get; set; }
        public string[] initial_states { get; set; }
        public string[] active_states { get; set; }
        //public string local_data { get; set; }
        //public byte[] local_data { get; set; }
        //public string info { get; set; }

        public SmachContainerStatus()
        {
            this.header = new Header();
            this.path = "";
            this.initial_states = new string[0];
            this.active_states = new string[0];
            //this.local_data = "";
            ////this.local_data = new byte[0];
            //this.info = "";
        }

        public SmachContainerStatus(Header header, string path, string[] initial_states, string[] active_states)
        {
            this.header = header;
            this.path = path;
            this.initial_states = initial_states;
            this.active_states = active_states;
            //this.local_data = local_data;
            //this.info = info;
        }

        //public SmachContainerStatus(Header header, string path, string[] initial_states, string[] active_states, string local_data, string info)
        //{
        //    this.header = header;
        //    this.path = path;
        //    this.initial_states = initial_states;
        //    this.active_states = active_states;
        //    this.local_data = local_data;
        //    this.info = info;
        //}

        //public SmachContainerStatus(Header header, string path, string[] initial_states, string[] active_states, byte[] local_data, string info)
        //{
        //    this.header = header;
        //    this.path = path;
        //    this.initial_states = initial_states;
        //    this.active_states = active_states;
        //    this.local_data = local_data;
        //    this.info = info;
        //}
    }
}
