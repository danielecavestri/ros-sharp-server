
using RosSharp.RosBridgeClient.MessageTypes.Logistic;

namespace RosSharp.RosBridgeClient.MessageTypes.CustomMsgs
{
    public class AddTwoIntsTestRequest : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/add_two_ints_test";

        public int num1 { get; set; }
        public int num2 { get; set; }
        public LogisticTask logistic_task { get; set; }

        public AddTwoIntsTestRequest()
        {
            this.num1 = 0;
            this.num2 = 0;
            this.logistic_task = new LogisticTask();
        }

        public AddTwoIntsTestRequest(int num1, int num2, LogisticTask logisticTask)
        {
            this.num1 = num1;
            this.num2 = num2;
            logistic_task = logisticTask;
        }
    }
}
