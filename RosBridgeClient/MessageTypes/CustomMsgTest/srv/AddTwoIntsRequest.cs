

namespace RosSharp.RosBridgeClient.MessageTypes.CustomMsgs
{
    public class AddTwoIntsRequest : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/add_two_ints";

        public int num1 { get; set; }
        public int num2 { get; set; }

        public AddTwoIntsRequest()
        {
            this.num1 = 0;
            this.num2 = 0;
        }

        public AddTwoIntsRequest(int num1, int num2)
        {
            this.num1 = num1;
            this.num2 = num2;
        }
    }
}
