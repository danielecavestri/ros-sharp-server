
namespace RosSharp.RosBridgeClient.MessageTypes.CustomMsgs
{
    public class AddTwoIntsResponse : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/add_two_ints";

        public int sum { get; set; }

        public AddTwoIntsResponse()
        {
            this.sum = 0;
        }

        public AddTwoIntsResponse(int sum)
        {
            this.sum = sum;
        }
    }
}
