
namespace RosSharp.RosBridgeClient.MessageTypes.CustomMsgs
{
    public class AddTwoIntsTestResponse : Message
    {
        public const string RosMessageName = "fanuc_cr14_moveit/add_two_ints_test";

        public int sum { get; set; }

        public AddTwoIntsTestResponse()
        {
            this.sum = 0;
        }

        public AddTwoIntsTestResponse(int sum)
        {
            this.sum = sum;
        }
    }
}
