﻿/*
© Siemens AG, 2019
Author: Berkay Alp Cakal (berkay_alp.cakal.ct@siemens.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Threading;
using System.Collections.Generic;

using RosSharp.RosBridgeClient.MessageTypes.ActionlibTutorials;
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;
using RosSharp.RosBridgeClient.MessageTypes.Logistic;

namespace RosSharp.RosBridgeClient.Actionlib
{
    public class LogisticActionServer : ActionServer<LogisticAction, LogisticActionGoal, LogisticActionResult, LogisticActionFeedback, LogisticGoal, LogisticResult, LogisticFeedback>
    {
        public string status = "";
        public string feedback = "";

        private ManualResetEvent isProcessingGoal = new ManualResetEvent(false);
        private Thread goalHandler;

        public LogisticActionServer(string actionName, RosSocket rosSocket, Log log)
        {
            this.actionName = actionName;
            this.rosSocket = rosSocket;
            this.log = log;
            action = new LogisticAction();
        }

        protected bool IsGoalValid()
        {
            return action.action_goal.goal.logistic_task != null && action.action_goal.goal.logistic_task.task_id != null;
        }

        private void ExecuteLogisticGoal()
        {
            isProcessingGoal.Set();


            // send LogisticTask to Hololens and wait for completion

            // Fake waiting
            Thread.Sleep(2000);



            SetSucceeded();
        }

        protected override void OnGoalReceived()
        {
            if (IsGoalValid())
            {
                SetAccepted("Logistic Action Server: The goal has been accepted");
            }
            else
            {
                SetRejected("Logistic Action Server: Cannot process a null LogisticTask");
            }
        }

        protected override void OnGoalRecalling(GoalID goalID)
        {
            // Left blank for this example
        }

        protected override void OnGoalRejected()
        {
            log("Cannot process a null LogisticTask. Goal Rejected");
        }

        protected override void OnGoalActive()
        {
            goalHandler = new Thread(ExecuteLogisticGoal);
            goalHandler.Start();
        }

        protected override void OnGoalPreempting()
        {
            isProcessingGoal.Reset();
            goalHandler.Join();
        }

        protected override void OnGoalSucceeded()
        {
            UpdateAndPublishStatus(ActionStatus.SUCCEEDED);
            isProcessingGoal.Reset();
            Thread.Sleep((int)timeStep * 1000);
            UpdateAndPublishStatus(ActionStatus.SUCCEEDED);
        }

        protected override void OnGoalAborted()
        {
            // Left blank for this example
        }

        protected override void OnGoalCanceled()
        {
            PublishResult();
        }
    }
}
