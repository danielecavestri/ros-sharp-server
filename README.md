# [Libraries](https://github.com/siemens/ros-sharp/tree/master/Libraries) #
 the .NET solution [RosSharp.sln](https://github.com/siemens/ros-sharp/tree/master/Libraries/RoRosSharp.sln) consists of the following projects:

 * [RosBridgeClient](https://github.com/siemens/ros-sharp/tree/master/Libraries/RosBridgeClient): .NET API to [ROS](http://www.ros.org/) via [rosbridge_suite](http://wiki.ros.org/rosbridge_suite)
 * [RosBridgeClientTest](https://github.com/siemens/ros-sharp/tree/master/Libraries/RosBridgeClientTest): NUnit and Console Application tests for RosBridgeClient
  * [MessageGeneration](https://github.com/sye8/ros-sharp/tree/master/Libraries/MessageGeneration): .NET Library for generating C# source code for ROS message/service/action.
 * [MessageGenerationMain](https://github.com/sye8/ros-sharp/tree/master/Libraries/MessageGenerationMain): Command line tool for generating C# source code for ROS message/service/action.
 * [UrdfImporter](https://github.com/siemens/ros-sharp/tree/master/Libraries/UrdfImporter): URDF file parser for .NET applications

Libraries `MessageGeneration.dll`, `RosBridgeClient.dll` and `UrdfImporter.dll` are required [plugins](https://github.com/siemens/ros-sharp/Unity3D/Assets/RosSharp/Plugins/) for the Unity project [Unity3D](https://github.com/siemens/ros-sharp/tree/master/Unity3D).

__Please see the [Wiki](https://github.com/siemens/ros-sharp/wiki) for further info.__

---

© Siemens AG, 2017-2018

Author: Dr. Martin Bischoff (martin.bischoff@siemens.com)

# Daniele's fork notes
Executable projects in the solution are:
* RosBridgeClientTest (native in the original repository, but I added and modified some scripts)
* Server (a project added by me)
* ExUnity (a project added by me)

## RosBridgeClientTest project
* RosSocketConsoleExample.cs: tests the communication with ros by subscribing topics and services
* FibonacciActionServerConsoleExample.cs: perform the same as the ros command `rosrun actionlib_tutorials fibonacci_server` but on C#. Anyway it has a bug reported [here](https://github.com/siemens/ros-sharp/issues/350)
* FibonacciActionClientConsoleExample.cs: perform the same as the ros command `rosrun actionlib_tutorials fibonacci_client` but on C#.
* LogisticActionServerConsoleExample.cs: it is meant to work with action client running on the state machine on ros (i.e. running `rosrun fanuc_cr14_moveit actions_smach.py`)
* ShapeActionClientConsoleExample.cs: it is the action client which calls the motion of a turtlesim running in ros; reference tutorial is [here](http://wiki.ros.org/actionlib_tutorials/Tutorials/Calling%20Action%20Server%20without%20Action%20Client) and I have just implemented it in C# without GUI.

## Server project
* ServerConsole.cs: it is the main script which I use to communicate both with ros and Hololens. For the moment it subscribes to `/current_logistic_task` published by `rosrun fanuc_cr14_moveit final_smach.py`

## ExUnity project
It contains the communication scripts that was in the previous ServerUnity project. Now in the server Unity is no more used, it is used only for Hololens development.
* MyServer.cs: it is the TCPListener which communicates with Hololens
