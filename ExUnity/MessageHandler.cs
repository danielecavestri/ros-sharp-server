﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RosSharp.RosBridgeClient.MessageTypes.Logistic;
using System.Threading;
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;

namespace ExUnity
{
    public class MessageHandler
    {
        private static Queue<string> messagesToHandle = new Queue<string>();
        public static CommunicationMessage initialMessage;
        public static Thread HandlingThread;

        public static GoalStatus operatorGoalStatus = new GoalStatus();


        public static void PushMessage(string jsonMessage)
        {
            messagesToHandle.Enqueue(jsonMessage);

            // it starts the thread only the first time that a message is pushed in the queue.
            if (HandlingThread == null || (HandlingThread != null && HandlingThread.ThreadState == ThreadState.Unstarted))
            {
                HandlingThread = new Thread(HandleMessages);
                HandlingThread.Start();
            }
        }

        private static void HandleMessages()
        {
            while (true)
            {
                if (messagesToHandle.Count > 0)
                {
                    string currentMessage = messagesToHandle.Dequeue();
                    if (currentMessage != null && !String.IsNullOrEmpty(currentMessage))
                    {
                        CommunicationMessage communicationMessage = JsonConvert.DeserializeObject<CommunicationMessage>(currentMessage);
                        switch (communicationMessage.MessageType)
                        {
                            case MessageType.LogisticTask:
                                LogisticTask logisticTask = (LogisticTask)communicationMessage.MessageContent;
                                break;
                            case MessageType.GoalStatus:
                                if (communicationMessage.Sender == "hololens")
                                {
                                    operatorGoalStatus = (GoalStatus)communicationMessage.MessageContent;
                                }
                                break;
                            case MessageType.RobotCommand:
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        //public static CommunicationMessage initialMessage;

        //public static void ModelInstantiationMessage()
        //{
        //    string jsonFileName = "test.json";
        //    //string jsonFileName = "completeConfig.json";
        //    string path = Path.Combine(new string[] { Application.dataPath, "ConfigurationFiles", jsonFileName });
        //    if (File.Exists(path))
        //    {
        //        using (StreamReader r = new StreamReader(path))
        //        {
        //            string json = r.ReadToEnd();
        //            initialMessage = ParseJson(json);
        //        }
        //    }
        //    else
        //    {
        //        Debug.LogError($"file '{path}' does not exist");
        //    }
        //    //InstantiationMessageContent message = (InstantiationMessageContent)ReadJson(path);
        //    //Debug.Log("message created: " + message.ToString());
        //    //return ReadJson(path);
        //}

        //public static CommunicationMessage ParseJson(string json)
        //{
        //    CommunicationMessage communicationMessage = JsonConvert.DeserializeObject<CommunicationMessage>(json);
        //    switch (communicationMessage.MessageType)
        //    {
        //        case MessageType.ModelInstantiation:
        //            //InstantiationMessageContent instantiationMessageContent = (InstantiationMessageContent)communicationMessage.MessageContent;
        //            communicationMessage.MessageContent = (InstantiationMessageContent)communicationMessage.MessageContent;
        //            //Debug.Log("instantiationMessageContent.AssetName: " + instantiationMessageContent.AssetName);
        //            return communicationMessage;
        //        case MessageType.RobotCommand:
        //            break;
        //        default:
        //            break;
        //    }
        //    return communicationMessage;
        //}

        //public static CommunicationMessage[] HandleLogisticTask(LogisticTask message)
        //{
        //    // IMPROVEMENTS: do not hardcode the operator's code
        //    //if (message.hardware_involved.Contains("h001"))
        //    //{
        //    //    CommunicationMessage[] communicationMessage = ParseRosMessage(message);
        //    //    return communicationMessage;
        //    //}
        //    //return null;

        //    CommunicationMessage[] communicationMessage = ParseRosMessage(message);
        //    return communicationMessage;
        //}

        //public static CommunicationMessage[] ParseRosMessage(LogisticTask message)
        //{
        //    CommunicationMessage[] communicationMessages = null;
        //    switch (message.task_type)
        //    {
        //        case "OPERATOR_WAREHOUSE_PICKING":
        //            communicationMessages = new CommunicationMessage[message.pieces_involved.Length];
        //            for (int i = 0; i < message.pieces_involved.Length; i++)
        //            {
        //                communicationMessages[i] = new CommunicationMessage("server", "hololens", MessageType.ModelInstantiation, null);
        //                //communicationMessages[i].MessageContent = new InstantiationMessageContent("pieces", message.pieces_involved[i], null, message.pieces_involved[i]);
        //                communicationMessages[i].MessageContent = new LogisticTask(message.task_id, message.task_name, message.task_type, new string[] { message.pieces_involved[i] }, message.hardware_involved);
        //            }
        //            break;
        //        case "ROBOT_COMMAND":
        //            break;
        //        default:
        //            break;
        //    }

        //    return communicationMessages;
        //}
    }
}