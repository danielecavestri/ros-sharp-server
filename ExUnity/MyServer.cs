﻿using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading;

namespace ExUnity
{

    // MonoBehaviour inheritance is used just for button interaction in Unity and for the bidirectional commuinication with graphical demonstration .
    public class MyServer
    {
        private string _ipAddress;
        //public string IpAddress;
        public int port = 8088;
        public IPEndPoint IPEndPoint { get; private set; }

        private TcpClient client;
        private NetworkStream stream;
        private Thread connectionThread;
        private Thread receiveThread;
        private Thread sendThread;

        private static ManualResetEvent connectionEvent = new ManualResetEvent(false);

        public static readonly Encoding Encoder = Encoding.UTF8;
        private static Queue<string> messagesToSend = new Queue<string>();



        //private Pose QRCodePose;
        //private Pose HeadPose;


        private bool exchangeStopRequested;

        public MyServer(string iPAddressString, string portString = "8088")
        {
            //this._ipAddress = ipAddress;
            //this.port = Int32.Parse(port);
            IPAddress iPAddress;
            bool IPisCorrect = IPAddress.TryParse(iPAddressString, out iPAddress);
            int port;
            bool portIsCorrect = Int32.TryParse(portString, out port);
            if (IPisCorrect && portIsCorrect && iPAddress != null && port != null && port > IPEndPoint.MinPort && port < IPEndPoint.MaxPort)
            {
                this.IPEndPoint = new IPEndPoint(iPAddress, port);

                exchangeStopRequested = false;
            }
            else
            {
                throw new FormatException("The IP is not a valid IP");
            }
        }

        public void StartServer()
        {
            //_ipAddress = IpAddress;
            //if (string.IsNullOrEmpty(_ipAddress))
            //{
            //    Console.WriteLine("Server IP not provided!");
            //    return;
            //}


            connectionThread = new Thread(Connect);
            connectionThread.Start();

            //if (this.IPEndPoint != null)
            //{

            //    Console.WriteLine("Connecting to " + this.IPEndPoint.ToString());

            //    client = new TcpClient();

            //    // ASYNC CONNECTION, WORKING 
            //    client.BeginConnect(this.IPEndPoint.Address, this.IPEndPoint.Port, OnConnectedToServer, null);
            //}



            //Console.WriteLine("Connecting to " + _ipAddress + " : " + port);
            //client = new TcpClient();

            //// ASYNC CONNECTION, WORKING 
            //client.BeginConnect(_ipAddress, port, OnConnectedToServer, null);

            // SYNC CONNECTION, WORKING
            //client.Connect(this._ipAddress, port);
            //if (client.Connected)
            //{
            //    print("Connected");
            //}
            //else
            //{
            //    print("Not Connected");
            //}
            //stream = client.GetStream();
            //RestartExchange();
            //Console.WriteLine("Client started!");
        }

        private void Connect()
        {
            if (this.IPEndPoint != null)
            {
                Console.WriteLine("Connecting to " + this.IPEndPoint.ToString());

                this.client = new TcpClient();

                // ASYNC CONNECTION, WORKING 
                this.client.BeginConnect(this.IPEndPoint.Address, this.IPEndPoint.Port, OnConnectedToServer, null);
            }
        }

        // Callback called when connection is established
        private void OnConnectedToServer(IAsyncResult result)
        {
            try
            {
                client.EndConnect(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine("client.EndConnect() error: " + ex.ToString());
            }
            if (client.Connected)
            {
                try
                {
                    stream = client.GetStream();
                    Console.WriteLine("Connected to " + client.Client.RemoteEndPoint);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("GetStream() error: " + ex.ToString());
                }
            }
            else
            {
                Console.WriteLine("Waiting connection with " + this.IPEndPoint.ToString());
            }
            if (stream != null)
            {
                RestartExchange();
            }
            else
            {
                this.client.BeginConnect(this.IPEndPoint.Address, this.IPEndPoint.Port, OnConnectedToServer, null);
            }
        }


        // A thread to send messages. With this separate thread the main thread is not stopped.
        // Known issues:
        //      this.messageToSendThread is the last message we want to be sent, but maybe for some delay the previous one
        //      is not sent yet. A better implementation could be to use a queue with FIFO logic.
        public void SendThread()
        {
            Console.WriteLine("SendThread() started...");
            while (!exchangeStopRequested)
            {
                if (stream != null && messagesToSend != null && messagesToSend.Count > 0)
                {
                    string message = messagesToSend.Dequeue();
                    Console.WriteLine("Sending: " + message);
                    //this.debugConsoleField.text += "Sending: " + messageToSend + "\n";
                    byte[] ByteMessageToSend = Encoder.GetBytes(message);
                    stream.Write(ByteMessageToSend, 0, ByteMessageToSend.Length);
                    Thread.Sleep(500);
                    //Console.WriteLine("Sent");
                    //this.debugConsoleField.text += "Sent\n";
                }
            }
        }

        // This function is not a thread
        public void SendJsonThread(object obj)
        {
            Console.WriteLine("SendJsonThread called");
            string msg = null;
            try
            {
                // convert to Json string serializing obj.
                msg = JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                //Console.WriteLine("Sending: " + msg);
            }
            catch (Exception ex)
            {
                string errorStatus = ex.ToString();
                Console.WriteLine("JsonConvert.SerializeObject: " + errorStatus);
            }
            if (msg != null && messagesToSend != null)
            {
                messagesToSend.Enqueue(msg);
            }
        }

        public void SendJsonTest() // function called by a button in the scene
        {
            Dictionary<string, double> Dict = new Dictionary<string, double>();
            Dict.Add("key1", 1.34);
            Dict.Add("key2", 1.34 * 2);
            Dict.Add("key3", 1.34 * 3);

            //this.SendJson(Dict);
            SendJsonThread(Dict);
        }

        public void RestartExchange()
        {
            if (receiveThread != null) StopExchange();
            exchangeStopRequested = false;
            receiveThread = new Thread(ReceivePacketsThread);
            receiveThread.Start();
            sendThread = new Thread(SendThread);
            sendThread.Start();
        }

        // A thread to receive messages. With this separate thread the main thread is not stopped.
        // Known issues:
        // 1) Sometime the variable "buf" contains multiple messages received from the server and concatenated in the "buf" array.
        //    This results in an Exception thrown in the this._coordReceiver.UpdatePoses() method. So these messages are lost.
        //    The reason maybe is some lag in the communication which results in multiple messages arriving in the same time.
        //    A solution could be to find the substrings "}{" or "][" in the message received (these substrings cause an exception
        //    in the Json Deserialization performed in this._coordReceiver.UpdatePoses(msg), then split the message with these substrings
        //    as delimiters and add the splitted messages to a queue of received messages and have a differente thread to analyze them.
        // 2) The decoding of messages received maybe is too structured (it is strongly typed). The decoding is performed in
        //    this._coordReceiver.UpdatePoses(msg) with JsonConvert.DeserializeObject<Dictionary<string, Pose>>(). 
        //    So the messages sent from the client must be, at the moment, of type Dictionary<string, Pose>.
        //    Maybe a more flexible implementation is to have messages with type Dictionary<string, object> with the first string
        //    that indicates the type of its correlated object (I don't know if it is possible).
        public void ReceivePacketsThread()
        {
            Console.WriteLine("ReceiveThread() started...");
            byte[] buf = new byte[49152];
            int bytesReceived = 0;

            while (!exchangeStopRequested && client != null && client.Connected && stream != null && stream.CanRead)
            {
                //Console.WriteLine("Start reading...");
                try
                {
                    bytesReceived = stream.Read(buf, 0, buf.Length);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Read() Exception:  " + ex.ToString(), true);

                    exchangeStopRequested = true;
                    this.StopExchange();
                    break;
                }
                //Console.WriteLine("End reading");
                if (bytesReceived > 0)
                {
                    string msg = Encoder.GetString(buf, 0, bytesReceived);
                    MessageHandler.PushMessage(msg);
                    //Console.WriteLine("Message received: " + msg);
                }
            }
            Console.WriteLine("Receiving stopped");
        }

        public void OnDestroy()
        {
            StopExchange();
        }

        public void StopExchange()
        {
            Console.WriteLine("Stopping exchange...");
            exchangeStopRequested = true;

            if (receiveThread != null)
            {
                //stream.Close();
                client.Client.Disconnect(true);
                //client.Close();

                //stream = null;
                //client = null;

                Console.WriteLine("Exchange stopped");
                this.client.BeginConnect(this.IPEndPoint.Address, this.IPEndPoint.Port, OnConnectedToServer, null);
                receiveThread.Abort();
                receiveThread = null;
            }
        }

    }
}