﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using RosSharp.RosBridgeClient.MessageTypes.Logistic;
using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;

namespace ExUnity
{
    public enum MessageType
    {
        LogisticTask,
        ActiveLogisticTasks,
        GoalStatus,
        ModelInstantiation,
        RobotCommand,
        ObjectPose
    }

    // Improvement for Sender attribute of CommunicationMessage class
    //public enum Devices
    //{
    //    Server,
    //    Hololens
    //}

    public class CommunicationMessage
    {
        public string Sender;
        public string Receiver;
        public MessageType MessageType;
        public Message MessageContent;

        public CommunicationMessage(string sender, string receiver, MessageType messageType, object messageContent)
        {
            Sender = sender;
            Receiver = receiver;
            MessageType = messageType;
            switch (MessageType)
            {
                case MessageType.ModelInstantiation:
                    try
                    {
                        MessageContent = JsonConvert.DeserializeObject<InstantiationMessageContent>(messageContent.ToString());
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine("DeserializeObject error: " + ex.ToString());
                    }
                    break;
                case MessageType.LogisticTask:
                    try
                    {
                        //MessageContent = JsonConvert.DeserializeObject<LogisticTask>(messageContent.ToString());
                        MessageContent = (LogisticTask)messageContent;
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine("DeserializeObject error: " + ex.ToString());
                    }
                    break;
                case MessageType.ActiveLogisticTasks:
                    try
                    {
                        //MessageContent = JsonConvert.DeserializeObject<LogisticTask>(messageContent.ToString());
                        MessageContent = (ActiveLogisticTasks)messageContent;
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine("DeserializeObject error: " + ex.ToString());
                    }
                    break;
                case MessageType.GoalStatus:
                    try
                    {
                        MessageContent = JsonConvert.DeserializeObject<GoalStatus>(messageContent.ToString());
                        //MessageContent = (GoalStatus)messageContent;
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine("DeserializeObject error: " + ex.ToString());
                    }
                    break;
                case MessageType.RobotCommand:
                    break;
                default:
                    break;
            }
        }
    }


    public class InstantiationMessageContent : Message
    {
        public string AssetBundleName;
        public string AssetName;
        public string AssetID;
        public string QRCodeID;

        public InstantiationMessageContent(string assetBundleName, string assetName, string assetID, string qRCodeID)
        {
            AssetBundleName = assetBundleName;
            AssetName = assetName;
            AssetID = assetID;
            QRCodeID = qRCodeID;
        }

        public override string ToString()
        {
            return "AssetBundleName: " + AssetBundleName + "\nAssetName:" + AssetName + "\nAssetID:" + AssetID + "\nQRCodeID:" + QRCodeID;
        }
    }
}