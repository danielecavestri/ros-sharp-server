﻿/*
© Siemens AG, 2019
Author: Berkay Alp Cakal (berkay_alp.cakal.ct@siemens.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Threading;
using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.Actionlib;
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;

namespace RosSharp.RosBridgeClientTest
{
    class ShapeActionClientConsoleExample
    {
        static readonly string uri = "ws://192.168.1.23:9090";
        static readonly string actionName = "turtle_shape";

        private static RosSocket rosSocket;
        private static ShapeActionClient shapeActionClient;

        public static void Main(string[] args)
        {
            //RosSocket rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketSharpProtocol(uri));
            rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketNetProtocol(uri));

            // Initialize Client
            shapeActionClient = new ShapeActionClient(actionName, rosSocket);
            shapeActionClient.Initialize();

            // Send goal
            Console.WriteLine("\nPress any key to send goal with fibonacci order 5...");
            Console.ReadKey(true);
            shapeActionClient.edges = 5;
            shapeActionClient.radius = 1;
            shapeActionClient.SendGoal();

            // Get feedback, status and result
            do
            {
                Console.WriteLine(shapeActionClient.GetFeedbackString());
                Console.WriteLine(shapeActionClient.GetStatusString());
                Thread.Sleep(100);
            } while (shapeActionClient.goalStatus.status != GoalStatus.SUCCEEDED);

            Thread.Sleep(500);
            Console.WriteLine(shapeActionClient.GetResultString());
            Console.WriteLine(shapeActionClient.GetStatusString());

            //// Cancel goal
            //Console.WriteLine("\nPress any key to send goal with fibonacci order 50...");
            //Console.ReadKey(true);
            //shapeActionClient.fibonacciOrder = 50;
            //shapeActionClient.SendGoal();

            //Console.WriteLine("\nPress any key to cancel the goal...");
            //Console.ReadKey(true);
            //shapeActionClient.CancelGoal();
            //Thread.Sleep(1000);
            //Console.WriteLine(shapeActionClient.GetResultString());
            //Console.WriteLine(shapeActionClient.GetFeedbackString());
            //Console.WriteLine(shapeActionClient.GetStatusString());

            // Terminate client
            shapeActionClient.Terminate();

            // End of console example
            Console.WriteLine("\nPress any key to close...");
            Console.ReadKey(true);
            rosSocket.Close();
        }

    }
}
