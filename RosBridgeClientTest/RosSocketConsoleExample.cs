﻿/*
© Siemens AG, 2017-2019
Author: Dr. Martin Bischoff (martin.bischoff@siemens.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using RosSharp.RosBridgeClient;

using std_msgs = RosSharp.RosBridgeClient.MessageTypes.Std;
using std_srvs = RosSharp.RosBridgeClient.MessageTypes.Std;
using rosapi = RosSharp.RosBridgeClient.MessageTypes.Rosapi;
using System.Net.Sockets;


// commands on ROS system:
// launch before starting:
// roslaunch rosbridge_server rosbridge_websocket.launch
// rostopic echo /publication_test
// rostopic pub /subscription_test std_msgs/String "subscription test message data"

// launch after starting:
// rosservice call /service_response_test

namespace RosSharp.RosBridgeClientTest
{
    public class RosSocketConsole
    {
        static readonly string uri = "ws://192.168.1.23:9090";
        //static readonly string uri = "ws://10.4.1.25:9090"; // Birex

        public static void OriginalMain()
        {
            //RosSocket rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketSharpProtocol(uri));
            RosSocket rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketNetProtocol(uri));

            // Publication:
            std_msgs.String message = new std_msgs.String
            {
                data = "publication test message data"
            };

            string publication_id = rosSocket.Advertise<std_msgs.String>("publication_test");
            rosSocket.Publish(publication_id, message);


            // Publication2:
            std_msgs.String message2 = new std_msgs.String
            {
                data = "subscription_test message data"
            };

            string publication_id2 = rosSocket.Advertise<std_msgs.String>("subscription_test");
            rosSocket.Publish(publication_id2, message2);

            Console.WriteLine("publishing topic");
            Console.WriteLine("topic published");

            // Subscription:
            string subscription_id = rosSocket.Subscribe<std_msgs.String>("/subscription_test", SubscriptionHandler);
            //subscription_id = rosSocket.Subscribe<std_msgs.String>("/subscription_test", SubscriptionHandler);

            // Service Call:
            rosSocket.CallService<rosapi.GetParamRequest, rosapi.GetParamResponse>("/rosapi/get_param", ServiceCallHandler, new rosapi.GetParamRequest("/rosdistro", "default"));

            // Service Response:
            string service_id = rosSocket.AdvertiseService<std_srvs.TriggerRequest, std_srvs.TriggerResponse>("/service_response_test", ServiceResponseHandler);

            Console.WriteLine("Press any key to unsubscribe...");
            Console.ReadKey(true);
            rosSocket.Unadvertise(publication_id);
            rosSocket.Unsubscribe(subscription_id);
            rosSocket.UnadvertiseService(service_id);

            Console.WriteLine("Press any key to close...");
            Console.ReadKey(true);
            rosSocket.Close();
        }

        public static void MyMain()
        {
            RosSocket rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketNetProtocol(uri));
            //RosSocket rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketNetProtocol(uri), RosSocket.SerializerEnum.Newtonsoft_JSON);



            // Subscription:
            string subscription_id = "";
            try
            {
                subscription_id = rosSocket.Subscribe<RosBridgeClient.MessageTypes.Logistic.LogisticTask>("/current_logistic_task", CustomMsgSubscriptionHandler);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Subscribe() error: " + ex.ToString());
            }

            Console.WriteLine("Press any key to unsubscribe...");
            Console.ReadKey(true);
            rosSocket.Unsubscribe(subscription_id);

            Console.WriteLine("Press any key to close...");
            Console.ReadKey(true);
            rosSocket.Close();
        }

        public static void MyMain2()
        {
            RosSocket rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketNetProtocol(uri));

            // Subscription:
            string subscription_id = "";
            try
            {
                subscription_id = rosSocket.Subscribe<std_msgs.Int32>("/client_count", ClientSubscriptionHandler);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Subscribe() error: " + ex.ToString());
            }


            Console.WriteLine("Press any key to unsubscribe...");
            Console.ReadKey(true);
            rosSocket.Unsubscribe(subscription_id);

            Console.WriteLine("Press any key to close...");
            Console.ReadKey(true);
            rosSocket.Close();
        }
        
        public static void MyMain3()
        {
            RosSocket rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketNetProtocol(uri));

            // Subscription:
            string subscription_id = "";
            try
            {
                subscription_id = rosSocket.Subscribe<RosBridgeClient.MessageTypes.Sensor.MyJointState>("/joint_states", JointStatesSubscriptionHandler);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Subscribe() error: " + ex.ToString());
            }


            Console.WriteLine("Press any key to unsubscribe...");
            Console.ReadKey(true);
            rosSocket.Unsubscribe(subscription_id);

            Console.WriteLine("Press any key to close...");
            Console.ReadKey(true);
            rosSocket.Close();
        }

        public static void Main(string[] args)
        {
            MyMain();
        }

        private static void JointStatesSubscriptionHandler(RosBridgeClient.MessageTypes.Sensor.MyJointState message)
        {
            foreach (string jointName in message.name)
            {
                Console.WriteLine("jointName: " + jointName);
            }
        }
        
        private static void ClientSubscriptionHandler(std_msgs.Int32 message)
        {
            Console.WriteLine("ClientSubscriptionHandler: " + (message).data);
        }
        
        private static void CustomMsgSubscriptionHandler(RosBridgeClient.MessageTypes.Logistic.LogisticTask message)
        {
            //Console.WriteLine("CustomMsgSubscriptionHandler: " + (message).active_states[0]);
            Console.WriteLine("CustomMsgSubscriptionHandler: " + (message).task_name);
        }

        private static void SmachSubscriptionHandler(RosBridgeClient.MessageTypes.Smach.SmachContainerStatus message)
        {
            Console.WriteLine("SmachSubscriptionHandler: " + (message).active_states[0]);
        }

        private static void SubscriptionHandler(std_msgs.String message)
        {
            Console.WriteLine("SubscriptionHandler: " + (message).data);
        }

        private static void ServiceCallHandler(rosapi.GetParamResponse message)
        {
            Console.WriteLine("ROS distro: " + message.value);
        }

        private static bool ServiceResponseHandler(std_srvs.TriggerRequest arguments, out std_srvs.TriggerResponse result)
        {
            result = new std_srvs.TriggerResponse(true, "service response message");
            return true;
        }
    }
}