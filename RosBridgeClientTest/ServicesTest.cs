﻿/*
© Siemens AG, 2017-2019
Author: Dr. Martin Bischoff (martin.bischoff@siemens.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using RosSharp.RosBridgeClient;
using std_msgs = RosSharp.RosBridgeClient.MessageTypes.Std;
using std_srvs = RosSharp.RosBridgeClient.MessageTypes.Std;
using rosapi = RosSharp.RosBridgeClient.MessageTypes.Rosapi;
using RosSharp.RosBridgeClient.MessageTypes.CustomMsgs;
using RosSharp.RosBridgeClient.MessageTypes.Logistic;
using RosSharp.RosBridgeClient.MessageTypes.Actionlib;
using System.Net.Sockets;


// commands on ROS system:
// launch before starting:
// roslaunch rosbridge_server rosbridge_websocket.launch
// rostopic echo /publication_test
// rostopic pub /subscription_test std_msgs/String "subscription test message data"

// launch after starting:
// rosservice call /service_response_test

namespace RosSharp.RosBridgeClientTest
{
    public class ServicesTest
    {
        static readonly string uri = "ws://192.168.1.23:9090";
        //static readonly string uri = "ws://172.17.112.222:9090"; // Birex


        public static void Main(string[] args)
        {
            //RosSocket rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketSharpProtocol(uri), RosSocket.SerializerEnum.Newtonsoft_JSON);
            RosSocket rosSocket = new RosSocket(new RosBridgeClient.Protocols.WebSocketNetProtocol(uri));

            // Service Response:
            //string service_id = rosSocket.AdvertiseService<AddTwoIntsRequest, AddTwoIntsResponse>("/add_two_ints", ServiceResponseHandler);
            //string service_id = rosSocket.AdvertiseService<AddTwoIntsTestRequest, AddTwoIntsTestResponse>("/add_two_ints", ServiceResponseHandler);
            string service_id = rosSocket.AdvertiseService<LogisticOperatorTaskRequest, LogisticOperatorTaskResponse>("/logistic_operator", ServiceResponseHandler);
            //string service_id = rosSocket.AdvertiseService<LogisticOperatorTaskNewRequest, LogisticOperatorTaskNewResponse>("/logistic_operator", ServiceResponseHandler);
            //string service_id = rosSocket.AdvertiseService<LogisticOperatorTaskTestRequest, LogisticOperatorTaskTestResponse>("/logistic_operator", ServiceResponseHandler);

            Console.WriteLine("Press any key to unsubscribe...");
            Console.ReadKey(true);
            rosSocket.UnadvertiseService(service_id);

            Console.WriteLine("Press any key to close...");
            Console.ReadKey(true);
            rosSocket.Close();
        }

        private static void ServiceCallHandler(rosapi.GetParamResponse message)
        {
            Console.WriteLine("ROS distro: " + message.value);
        }


        private static bool ServiceResponseHandler(AddTwoIntsRequest arguments, out AddTwoIntsResponse result)
        {
            result = new AddTwoIntsResponse(arguments.num1 + arguments.num2);
            Console.WriteLine($"{arguments.num1} + {arguments.num2} = {result.sum}");
            return true;
        }
        
        private static bool ServiceResponseHandler(AddTwoIntsTestRequest arguments, out AddTwoIntsTestResponse result)
        {
            result = new AddTwoIntsTestResponse(arguments.num1 + arguments.num2);
            Console.WriteLine($"{arguments.num1} + {arguments.num2} = {result.sum}");
            return true;
        }

        private static bool ServiceResponseHandler(LogisticOperatorTaskRequest request, out LogisticOperatorTaskResponse result)
        {
            //result = new LogisticOperatorTaskResponse(new LogisticActionResult(new std_msgs.Header(), new GoalStatus(new GoalID(new std_msgs.Time(), "logistic_operator_goal_id"), GoalStatus.SUCCEEDED, ""), new LogisticResult()));
            result = new LogisticOperatorTaskResponse();
            //result.action_result.status.status = GoalStatus.SUCCEEDED;
            result.action_result.status.status = GoalStatus.ABORTED;

            Console.WriteLine($"setting {request.logistic_task.task_id} to {result.action_result.status.status}");
            return true;
        }

        private static bool ServiceResponseHandler(LogisticOperatorTaskNewRequest request, out LogisticOperatorTaskNewResponse result)
        {
            //result = new LogisticOperatorTaskResponse(new LogisticActionResult(new std_msgs.Header(), new GoalStatus(new GoalID(new std_msgs.Time(), "logistic_operator_goal_id"), GoalStatus.SUCCEEDED, ""), new LogisticResult()));
            result = new LogisticOperatorTaskNewResponse();
            result.status.status = GoalStatus.SUCCEEDED;

            Console.WriteLine($"setting {request.LogisticTask.task_id} to {result.status.status}");
            return true;
        }

        private static bool ServiceResponseHandler(LogisticOperatorTaskTestRequest request, out LogisticOperatorTaskTestResponse result)
        {
            result = new LogisticOperatorTaskTestResponse(true);
            //result.succeeded = true;

            Console.WriteLine($"setting {request.LogisticTask.task_id} to {result.succeeded}");
            return true;
        }
    }
}